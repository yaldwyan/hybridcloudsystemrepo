# Adaptive Virtual Infrastructure Management System in Hybrid Cloud Environment (AVIMS) - README  #

This system provides fault tolerant techniques in hybrid cloud environments that enable those environments to optimize the utilization of resources and be efficiently adaptive in the presence of failures.



## Key features: ##
* AVIMS deploys cloud services, e.g creating instances of virtual machines (VMs) and volumes, in different cloud platforms e.g Nectar and Amazon clouds.
* It automates the installation of cloud applications and their dependencies on VMs.
* It supports Openstack-based Nectar, Amazon and Google Compute cloud platforms.
* Other cloud platforms can be easily supported.
* It supports only Linux as an operating system running on VMs.
* It detects failures and recover from them. It supports a failure model in the the following table:

Failure                        | Detection                          | Recovery 
----------------------- ---    | ---------------------------------- | -------------------------------------------------------- 
Full Private VM Pool Capacity  | No idle VMs in private cloud       | Launch VMs from public clouds
VM Crash                       | Ping                               | Launch a new VM in private or public clouds
VM Slowdown                    | Response time is high              | Launch a new VM in private or public clouds
VM Highload                    | CPU or memory utilization is high  | Launch a new VM to distribute the load
VM Lowload                     | CPU or memory utilization is low   | Decrease the number of running VMs
Full VM Volume                 | Volume space is highly used        | Maximize size of Volume for VM (Only single cloud solution)


>  AVIMS was tested on Ubuntu 14.04 and 15.10. A cloud application for the test was a simple web server.

### How do I get set up? ###

* Configuration
    1. Create a directory for AVIMS e.g. AVIMS_DIR
    2. Create AVIMS_DIR/src directory, and put all source files and folders in it. 
    3. Edit credentials to each CLOUDNAME_utils file, e.g. for amazon, edit them to amazon_utils.py. 
    4. Create AVIMS_DIR/key directory; put all private keys for clouds that will be used. These keys allow AVIMS to install a cloud agent and cloud applications in virtual machines (VMs) immediately after boot time.
    5. In each CLOUDNAME_utils file:
        1. Edit a key-pair name, a name of previously created key-pair to be injected into the instance, e.g. in Nectar, assign `NECTAR_KEY_PAIR_NAME` field your Nectar key-pair name. 
        2. Edit a private key name e.g. in Nectar, assign `NECTAR_PRIVATE_KEY` field your Nectar private key name.

    7. Edit an availability zones, or regions, security groups, instance type and image id in each CLOUDNAME_utils file.

* Dependencies
    1. Install [OpenStack Python SDK](http://docs.openstack.org/user-guide/sdk.html)
    2. Install [Boto: A Python interface to Amazon Web Services](https://boto.readthedocs.org/en/latest/)
    3. Install [Google API Client for Python](https://cloud.google.com/compute/docs/tutorials/python-guide)
    4. Install [Paramiko: a Python (2.6+, 3.3+) implementation of the SSHv2 protocol](http://www.paramiko.org/)


```
#!bash
$ pip install python-openstackclient
$ pip install boto
$ pip install --upgrade google-api-python-client
$ pip install paramiko

```

* How to run tests

Failure                        | Test Procedure
----------------------- ---    | ------------------------------------------------------------------------------------------- 
VM Crash                       | Terminate the target VM manually from the dashboard of the cloud console
VM Slowdown                    | Run: `python experiment.py 'slowdowntest' 'TARGET_VM_IP'` 
VM Highload                    | Run: `python experiment.py 'highloadtest' 'TARGET_VM_IP'` 
VM Lowload                     | Run: `python experiment.py 'lowloadtest' 'TARGET_VM_IP'` 
Full VM Volume                 | Run: `python experiment.py 'fullvolumetest' 'TARGET_VM_IP'` 

> Remember: TARGET_VM_IP has to be replaced with the IP address of the target VM.

* Deployment instructions

Simply run `python deploy.py` 

If you want to enable/disable a specific cloud, comment/uncomment that cloud object in `initializeResourcePools()` function in deploy.py file.



### How do I add (implement) a new cloud?  ###

1. Create a utility and configuration file of the new cloud, preferably name it NEWCLOUDNAME_utils.py file.
2. Add all configurations needed to connect to the new cloud and its VMs, such as credentials, key-pair name and so forth.
3. Add `NEWCLOUDNAME_CLOUD='newcloud'` in cloudutils.py file, e.g. `GOOGLE_CLOUD='google'`.
4. Add the new cloud implementation at the end of each method in `ResourcePool` class listed in the following table: 

    | Function name               | Service                        | Comment
    | -------------               | ------------------------------ |------------------------------------
    | `launchVM()`                | Compute                        | 
    | `terminateVM()`             | Compute                        | 
    | `createAndAttachVolume()`   | Storage (volume)               | To deal with volumes
    | `attachVolume()`            | Storage (volume)               | 
    | `detachVolume()`            | Storage (volume)               | 
    | `resizeVolume()`            | Storage (volume)               | To recover from a 'full VM volume' failure

5. In `initializeResourcePools()` function in deploy.py file: 
    1. create a new object of `ResourcePool` class for the new cloud and specify the type of cloud, available number of VMs, available volume size and vendor name (i.e. cloud name in step 3). 
    2. Add a resource pool to resource pool list in provisioner module.
For instance,

```
#!python
    '''Initializing Amazon cloud as a public resource pool'''    
    googlePubResPool  = ResourcePool(resPoolId=resourcepool.generateResourcePoolId(), 
                                     resPoolName="Google Compute Cloud", 
                                     resPoolType=cloudutils.PUBLIC,
                                     numVms=15, volumeSize=200, 
                                     vendor=cloudutils.GOOGLE_CLOUD)
    provisioner.addPublicResourcePool(googlePubResPool)  

```




### Who do I talk to? ###
Yasser Aldwyan (yasser.dwyan@gmail.com)