'''
This module deploys a hybrid cloud manager system. It initializes resource pool 
objects which launch and terminate virtual machines (VMs) and create, attach and
detach volumes to VMs in different clouds. 

If a new public cloud implemented in Resource Pool class, simply create 
a new ResourcePool object for it in initializeResourcePools function. 
resPoolId and vendor are very important to distinguish among different clouds 
in the hybrid cloud manager system. 

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import failurerecovery, failuredetection, cloudinterface, clouduser, cloudutils
import monitoring, provisioner 
from resourcepool import ResourcePool
import logging
import resourcepool

 
def initializeResourcePools():    
    '''Initializing the private cloud i.e. in our case'''
    privResPool = ResourcePool(resPoolId=resourcepool.generateResourcePoolId(), 
                               resPoolName="Nectar Research Cloud", 
                               resPoolType=cloudutils.PRIVATE,
                               numVms=2, volumeSize=120, 
                               vendor=cloudutils.NECTAR_CLOUD)
    provisioner.setPrivateResourcePool(privResPool)   
    '''Initializing Amazon cloud as a public resource pool'''    
    amazonPubResPool  = ResourcePool(resPoolId=resourcepool.generateResourcePoolId(),
                                     resPoolName="Amazon Cloud",
                                     resPoolType=cloudutils.PUBLIC,
                                     numVms=1, volumeSize=0, 
                                     vendor=cloudutils.AMAZON_CLOUD)
    provisioner.addPublicResourcePool(amazonPubResPool) 
    '''Initializing Amazon cloud as a public resource pool'''    
    googlePubResPool  = ResourcePool(resPoolId=resourcepool.generateResourcePoolId(), 
                                     resPoolName="Google Compute Cloud", 
                                     resPoolType=cloudutils.PUBLIC,
                                     numVms=1, volumeSize=0, 
                                     vendor=cloudutils.GOOGLE_CLOUD)
    provisioner.addPublicResourcePool(googlePubResPool)  
    
    '''
    if a new public cloud implemented in Resource Pool class, simply create 
    a new ResourcePool object for it here. resPoolId and vendor are very 
    important to distinguish among different clouds in the hybrid cloud manager 
    system. 
    '''
    print "All resource pools are ready in all clouds."
    privCloud = provisioner.privResPool
    print "Private Cloud: {}, number of resources:".format(privCloud.resPoolName,
                                                           privCloud.getNumVms())
    publicPools = provisioner.pubResPools
    print "Number of public clouds: {}".format(str(len(publicPools)))
    for pp in publicPools:
        print 'Public Cloud: {}, Number of resources: {}'.format(pp.resPoolName,
                                                                 pp.getNumVms())



if __name__ == '__main__':
    logging.basicConfig(filename="hybridcloud.log", level=logging.INFO,
                        format='%(asctime)s %(message)s')
    threads = []
    print "starting cloud manager deployment..."
    logging.info("starting cloud manager deployment...")  
    print 'Starting monitoring...'
    logging.info("starting cloud manager deployment...")
    monitorThread = monitoring.startMonitoringService()
    threads.append(monitorThread)  
    failurerecovery.startFailureRecoveryService()   
    failuredetection.startFailureDetectionService()   
    print "Initializing resource pools..."
    logging.info("Initializing resource pools...")
    initializeResourcePools()
    
    '''Here a user request for vms is created. This should be received by a web
    interface for cloud interface.'''   
    #Start the experiment
    print "Creating a user request"
    logging.info("Creating a user request")
    '''This can be handled in a web service in cloud interface component'''
    request = clouduser.UserRequest(reqName="Request1", numVms=2,
                          minVms=2, maxVms=4, 
                          highLoadThr=0.8,lowLoadThr=0.2, volumeThr=0.8,
                          vmSize=cloudutils.VM_MEDUIM, 
                          responseTimeThr=5,
                          hasLb=True, lbPort=80, vmPort=8080)
    ''' if you need a volume, add the following arguments to UserRequest Object:
    hasVolume=True and volumeSize=20'''   
    print "Passing the user request to cloud interface to start handle request scenario"
    cloudinterface.receiveRequest(request)    
    '''if you want to exit the cloud manager: type 'exit' in the console'''
    while True:
        key = raw_input("Enter exit to exit:")
        if key == "exit":
            '''Shutting down all running components in hybrid cloud manager.'''
            cloudutils.exitCloudManager()
            monitoring.stopMonitoringService()
            '''Terminate all running VMs in all clouds.'''
            priResPool = provisioner.privResPool
            if len(priResPool.vms) > 0:
                for vm in priResPool.vms:
                    priResPool.terminateVM(vm.provVmId)            
            pubRPools = provisioner.pubResPools
            for pubRPool in pubRPools:
                if len(pubRPool.vms) > 0:
                    for vm in pubRPool.vms:
                        pubRPool.terminateVM(vm.provVmId)         
            print "All VMs terminated."       
            break
    print "Cloud manager shut down."