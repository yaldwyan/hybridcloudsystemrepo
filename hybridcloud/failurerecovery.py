'''
This module simply handles failures. The way of recovering from failures depend 
mainly on the nature of failure and the cloud policies received initially from 
Cloud Services and Policies component for a cloud application. 
For instance, a crash failure can be recovered by sending a new launch request 
to Provisioner.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import Queue, logging, threading, time
import cloudutils, failure, provisioner

'''
Failure recovery message queue.The failure detection service puts failure 
messages in the queue.
'''
frMsgQue = Queue.Queue()


class FailureRecoveryThread(threading.Thread):
    '''
    This class from any failure in the proposed hybrid cloud system. 
    It runs in a separate threads. It reads messages from failure recovery 
    message queue.
    '''
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = 'MessageBasedFailureDetection Thread'
    
    def run(self):
        while not cloudutils.exitFlag:
            if not frMsgQue.empty():
                fdMsg = frMsgQue.get()
                frh = FailureRecoveryHandlerThread(fdMsg)
                frh.start()              
        print "{} terminated.".format(self.name)



class FailureRecoveryHandlerThread(threading.Thread):
    '''
    This class handles each failure in a separate thread. 
    '''
    def __init__(self, fdMsg):
        threading.Thread.__init__(self)
        self.failureMsg = fdMsg
    
    def run(self):
        recoverFromFailure(self.failureMsg)
        
'''
This function does the main job to recover from a failure in the hybrid cloud
system.
'''
def recoverFromFailure(failureMsg):
    asService = failureMsg.asService
    logging.info("Failure Recovery: a failure message received. Failure type: {}".format(failureMsg.failureType)) 
    print "Failure Recovery: a failure msg received. Failure type: {}".format(failureMsg.failureType)   
    
    if failureMsg.failureType == failure.VM_CRASH_FAILURE:
        print "inside if crash_FAlure"
        '''Unregister failed vm from LB if the job has an loadbalancer'''
        if asService.hasLb:
            asService.lb.unregisterVM(failureMsg.failedVm)
        newVM = provisioner.terminateFailedVmAndLaunchNewVm(failureMsg.failedVm,
                                                            failureMsg.asService)
        print "New VM IP: {}".format(newVM.ip)
        if asService.hasLb:
            asService.lb.registerVM(newVM)
            
    elif failureMsg.failureType == failure.VM_SLOWDOWN_FAILURE:
        newVM = provisioner.launchNewVm(failureMsg.asService)
        if asService.hasLb:
            asService.lb.registerVM(newVM)
            time.sleep(0.1)
            asService.lb.unregisterVM(failureMsg.failedVm)
        time.sleep(0.2)
        provisioner.releaseVM(failureMsg.failedVm, asService)
        
    elif failureMsg.failureType == failure.VM_HIGHLOAD_FAILURE:
        newVM = provisioner.launchNewVm(failureMsg.asService)
        if asService.hasLb:
            asService.lb.registerVM(newVM)
            
    elif failureMsg.failureType == failure.VM_LOWLOAD_FAILURE:
        if len(asService.vms) > 1:
            targetVm = findPublicVm(asService)
            if not targetVm:
                targetVm = asService.vms[0]
            if asService.hasLb:
                asService.lb.unregisterVM(targetVm)
            provisioner.releaseVM(targetVm, asService)
            
    elif failureMsg.failureType == failure.VM_STORAGE_SHORTAGE_FAILURE:
        volume = failureMsg.failedVm.getVolume()
        newSize = volume.getSize() + 5
        provisioner.resizeVolume(failureMsg.failedVm, newSize)
        
    else:
        logging.info("Failure Recovery: Unknown failure type.")
        print "Failure Recovery: Unknown failure type."
        
    failureMsg.timestampeEndRecoveryTime()
    asService.setStatus(cloudutils.HEALTHY)
    logging.info("Failure Recovery: a failure recovered. Failure type: {}, Time to Recovery: {}"
                 .format(failureMsg.failureType, failureMsg.getTimeToRecovery())) 
    print "Failure Recovery: a failure recovered. Failure type: {}, Time to Recovery: {}".format(failureMsg.failureType, failureMsg.getTimeToRecovery())       


    
'''
This function is used to find a public VM, if applicable, running a cloud 
application. In this way we reduce the monetary cost since we choose public VMs
to terminate. 
'''
def findPublicVm(asService):
    priProviderId = provisioner.privResPool.getId()
    targetVm = None
    for vm in asService.vms:
        if vm.resPoolId != priProviderId:
            targetVm = vm
            break
    return targetVm


'''
This method should be called to run failure recovery service
'''            
def startFailureRecoveryService():
    frThr = FailureRecoveryThread()
    frThr.start()
    logging.info( '{} started.'.format(frThr.name) )
    

        
if __name__ == '__main__':
    pass