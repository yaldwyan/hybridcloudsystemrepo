'''
This module defines a web server, which runs at a back-end VM, that is used in
our hybrid cloud system.

This is a cloud application in our system.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import SimpleHTTPServer, SocketServer, logging, time
    
    
class WebServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    '''
    This a web server handler to handle requests coming from clients
    '''

    def do_GET(self):
        
        time.sleep(getSleepTime())
        if self.path == '/':
            self.path = '/index.html'
        
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)        

class ThreadedHTTPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass    
    
'''Set sleep time'''  
_sleeptime = 0
def setSleepTime(st):
    global _sleeptime 
    _sleeptime = st
    logging.info("Sleeping time changed, now it is {}".format(_sleeptime))
    
def getSleepTime():
    global _sleeptime
    return _sleeptime


def startWebServerLog():
    logging.basicConfig(filename="webserver.log", level=logging.INFO,
                        format='%(asctime)s %(message)s')
    logging.info("Web server is starting.")



if __name__ == '__main__':
    pass 