'''
This module deals with user requests for VMs coming from users. It can be expanded
to have a web service to receive requests. 

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''
import cloudservicepolicy

'''Receive a user request and then pass it to cloud service and policy component.
'''
def receiveRequest(request):
    cloudservicepolicy.handleUserRequest(request)