'''
This module periodically receives VM health messages, extract the health 
information e.g. CPU load and send it to Detection component.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import SocketServer, threading

import cloudutils, failuredetection


monitorServer = None

class MonitorRequestHandler(SocketServer.BaseRequestHandler):
    '''
    This class handles a message coming from a running VM.
    '''
    def handle(self):
        jMsg = self.request.recv(1024)
        failuredetection.fdMsgQue.put(jMsg)


              
class MonitoringServer(SocketServer.ThreadingMixIn,
                        SocketServer.TCPServer):
    pass



'''
This function starts monitoring service.
'''
def startMonitoringService():
    HOST = ''
    global monitorServer
    monitorServer = MonitoringServer((HOST, cloudutils.MONITORING_PORT),
                               MonitorRequestHandler)
    ip, port = monitorServer.server_address
    global monitor_thread
    monitor_thread = threading.Thread(target=monitorServer.serve_forever)
    monitor_thread.start()
    print "Monitoring service is running now.", monitor_thread.name
    print "Host, Port: ", ip, port
    return monitor_thread


'''
This function stop monitoring service.
'''
def stopMonitoringService():
    global monitorServer
    monitorServer.shutdown()
    print "Monitoring service stopped."

    
        
if __name__ == '__main__':
    pass    