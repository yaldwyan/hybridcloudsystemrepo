'''
This file contains all configurations needed to connect to Google Compute Cloud.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import time


GOOGLE_KEY = 'YOUR_GOOGLE_KEY'
GOOGLE_PROJECT = 'YOUR_GOOGLE_PROJECT_NAME'
GOOGLE_ZONE = 'asia-east1-a'
SOURCE_DISK_IMAGE = '/projects/ubuntu-os-cloud/global/images/ubuntu-1404-trusty-v20160314'
MACHINE_TYPE = "zones/%s/machineTypes/n1-standard-2" % GOOGLE_ZONE
USER_NAME = 'YOUR_USER_NAME'



'''
This function helps to wait until the operation in Google Compute Cloud is done.
'''
def waitForOperation(compute, project, zone, operation):
    print('Waiting for operation to finish...')
    while True:
        result = compute.zoneOperations().get(
            project=project,
            zone=zone,
            operation=operation).execute()

        if result['status'] == 'DONE':
            print("done.")
            if 'error' in result:
                raise Exception(result['error'])
            return result

        time.sleep(1)


        
if __name__ == '__main__':
    pass