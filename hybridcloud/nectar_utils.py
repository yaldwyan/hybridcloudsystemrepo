'''
This file contains all configurations and utilities needed to connect to NeCtar 
Cloud.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

OS_USERNAME = "YOUR_NECTAR_USERNAME"
OS_PASSWORD = "YOUR_NECTAR_PASSWORD"
OS_AUTH_URL = "https://keystone.rc.nectar.org.au:5000/v2.0/"
OS_TENANT_NAME = "YOUR_NECTAR_PROJECT_NAME"
NECTAR_KEY_PAIR_NAME= "YOUR_NECTAR_KEY_PAIR_NAME"
NECTAR_PRIVATE_KEY = 'YOUR_NECTAR_PRIVATE_KEY'
AVAILABILITY_ZONE="melbourne-np"


MEDUIME_SIZE="m1.medium"


VOLUME_STATUS_AVAILABLE = 'available'
VOLUME_STATUS_ATTACHING = 'attaching'
VOLUME_STATUS_IN_USE = 'in-use'

UBUNTU_14_04_TRUSTY_IMAGE_ID = 'e9f0323e-9383-4dc9-a2ee-846ef8d35ee7'
UBUNTU_15_10_WILY_IMAGE_ID = '31ccebc4-ae2d-4f6d-8e3a-57b5bce533be'

'''
This function helps to get credentials in order to connect to nova easily.
'''
def get_nova_credentials_v2():
    d = {}
    d['version'] = '2'
    d['username'] = OS_USERNAME 
    d['api_key'] = OS_PASSWORD 
    d['auth_url'] = OS_AUTH_URL
    d['project_id'] = OS_TENANT_NAME
    return d

'''
This function get IP address from instance object returned from NeCtart Cloud.
'''
def getIpAdd(instance):
        addressList = instance.addresses
        networkLabels = addressList.keys()
        
        firstNework = addressList[networkLabels[0]]
         
        for add in firstNework:
            ip = add['addr']
            
        return ip 
    
