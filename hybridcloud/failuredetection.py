'''
This module detects failures that may occur during the lifetime of cloud 
applications running on VMs in different clouds.

It can determine that a failure occurs based on the VM health information coming
from VMs via Monitoring service, or information obtained by direct communications 
with VMs e.g. pinging and measuring response times.

Each detection methods run at different threads.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import cloudservicepolicy, cloudutils, failure, failurerecovery, message
import Queue, httplib, json, logging, subprocess, threading, time


'''
Failure detection message queue for message based failure detection methods.
The monitoring service puts health messages that come from cloud agent running
on VMs in the queue.
'''
fdMsgQue = Queue.Queue()

'''
This function should be called to run failure detection service
'''
def startFailureDetectionService():
    msgFDThr = MsgBasedFailureDetectionThread()
    msgFDThr.start()
    print '{} started.'.format(msgFDThr.name)
    logging.info( '{} started.'.format(msgFDThr.name) )
    
    crashFDThr = CrashFailureDetectionThread()
    crashFDThr.start()
    logging.info( '{} started.'.format(crashFDThr.name) )
    print '{} started.'.format(crashFDThr.name)
    
    #Response time failure thread here
    slowdownFDThr = SlowdownFailureDetectionThread()
    slowdownFDThr.start()
    logging.info( '{} started.'.format(slowdownFDThr.name) )
    print '{} started.'.format(slowdownFDThr.name)
    


class MsgBasedFailureDetectionThread(threading.Thread):
    '''
    This class detects any failure that is based on health messages. It runs in
    a separate threads. It reads messages from failure detection message queue.
    '''
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = 'MessageBasedFailureDetection Thread'
    
    def run(self):
        while not cloudutils.exitFlag:
            if not fdMsgQue.empty():
                jMsg = fdMsgQue.get()
                detectMsgBasedFailure(jMsg)          
        print "{} terminated.".format(self.name)
        
'''
This function does the main job for message based failure detection method
'''        
def detectMsgBasedFailure(jMsg):
    msg = json.loads(jMsg)
    vmId = msg[message.ID]
    asService = cloudservicepolicy.lookupAsServiceByVmId(vmId)
    if asService:
        if asService.getStatus() == cloudutils.HEALTHY:
            isFailed = False            
            currVm = asService.getVmObj(vmId)
            policy = asService.policy
            vmCpu = msg[message.CPU_UTILIZATION]
            vmMem = msg[message.MEMORY_UTILIZATION]
            
            if message.USED_VOLUME_SPACE in jMsg:
                vmUsedVol = msg[message.USED_VOLUME_SPACE]
            else:
                vmUsedVol = None
            
            failureMsg = None   
            if vmCpu > policy.getHighLoadThr():
                failureMsg = failure.Failure(failure.VM_HIGHLOAD_FAILURE, asService)
                failurerecovery.frMsgQue.put(failureMsg)
                isFailed = True
            elif vmCpu < policy.getLowLoadThr(): 
                failureMsg = failure.Failure(failure.VM_LOWLOAD_FAILURE, asService)
                failurerecovery.frMsgQue.put(failureMsg)
                isFailed = True
                
            if vmUsedVol:
                if vmUsedVol > policy.getVolumeThr():
                    failureMsg = failure.Failure(failure.VM_STORAGE_SHORTAGE_FAILURE,
                                                             asService,
                                                             failedVm=currVm)
                    failurerecovery.frMsgQue.put(failureMsg)
                    isFailed = True
                    
            if isFailed:
                asService.setStatus(cloudutils.UNHEALTHY)
                logging.info("Failure Detection: A {} failure is detected.".format(failureMsg.failureType))
                print "Failure Detection: A {} failure is detected.".format(failureMsg.failureType)
    else:
        print "FD: detectMsgBasedFailure(): No asService."
        


class CrashFailureDetectionThread(threading.Thread):
    '''
    This class detects any crash failure by pinging to all running VMs. It runs 
    in a separate threads.
    '''
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = 'CrashFailureDetection Thread'
    
    def run(self):
        detectCrashFailure()
        print "{} terminated.".format(self.name)
                

'''
This function does the main job for detecting crash failures.
'''       
def detectCrashFailure():
    while not cloudutils.exitFlag:
        asServiceList = cloudservicepolicy.asServices
        if (len(asServiceList) > 0):
            for asService in asServiceList:
                vmList = asService.vms
                for vm in vmList:
                    if vm.status == cloudutils.RUNNING:
                        numReceievedPackets = ping(vm.ip)
                        if numReceievedPackets == 0:
                            '''retry'''
                            numReceievedPackets = ping(vm.ip)
                            if numReceievedPackets == 0:
                                vm.status = cloudutils.CRASHED
                                asService.setStatus(cloudutils.UNHEALTHY)
                                crashFailure = failure.Failure(failure.VM_CRASH_FAILURE,
                                                               asService)
                                crashFailure.failedVm = vm                    
                                failurerecovery.frMsgQue.put(crashFailure)
                                logging.info("Failure Detection: A crash failure is detected.")
                                print "Failure Detection: A crash failure is detected."                                
        time.sleep(3)



''' Checking response time of all running vms running as back-end servers '''
class SlowdownFailureDetectionThread(threading.Thread):
    '''
    This class detects any slowdown failure by measuring response times of all 
    running VMs as back-end servers. It runs in a separate threads.
    '''
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = 'SlowdownFailureDetection Thread'
    
    def run(self):
        detectSlowdownFailure()
        print "{} terminated.".format(self.name)
                
                        
'''
This function does the main job for detecting slowdown failures.
'''        
def detectSlowdownFailure():
    while not cloudutils.exitFlag:
        asServiceList = cloudservicepolicy.asServices
        if (len(asServiceList) > 0):
            for asService in asServiceList:
                serverPort = asService.lb.vmPort
                vmList = asService.vms
                for vm in vmList:
                    if  vm.status == cloudutils.RUNNING and not vm.isLb:                                                     
                        responseTime = measureResponseTime(vm.ip, serverPort)
                        if responseTime > asService.policy.getResponseTimeThr():
                            vm.status = cloudutils.FAILED
                            asService.setStatus(cloudutils.UNHEALTHY)
                            slowdownFailure = failure.Failure(failure.VM_SLOWDOWN_FAILURE,
                                                               asService)
                            slowdownFailure.failedVm = vm
                            failurerecovery.frMsgQue.put(slowdownFailure)
                            logging.info("Failure Detection: A slowdown failure is detected. Ip: {}".format(vm.ip))
                            print "Failure Detection: A slowdown failure is detected.  Ip: {}".format(vm.ip)   
                    elif vm.status == cloudutils.FAILED and not vm.isLb:
                        responseTime = measureResponseTime(vm.ip, serverPort)
        time.sleep(3)
        
        

'''
================================================================================
                            Helper functions
================================================================================
'''
        
'''
This function measures a response time of an http server
'''
def measureResponseTime(ip, port):
    
    startTime = time.time()
    try:
        httpReq = httplib.HTTPConnection(ip, port, timeout=12)
        httpReq.request("GET", "/")   
        response = httpReq.getresponse()
        data = response.read()
    except:
        print "FD: Failed to connect to {} on port {} to measure response time.".format( ip, str(port) )
        logging.info("FD: Failed to connect to {} on port {} to measure response time.".format( ip, str(port) ))
        return -1    
    endTime = time.time()
    responseTime = endTime - startTime
    print "measureResponseTime() ip:{}, port:{}, response time: {}".format(ip,port,responseTime)  
    return responseTime
    


'''
This function pings  any remote machines. 
return non-zeor valure if remote machine is alive, or 0 if it is crashed
'''
def ping(ip):
    cmd = "ping -c1 -w3 " + ip   
    outputdata, errordata = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE, 
                                            shell=True).communicate() 
    lines = outputdata.splitlines()
    for line in lines:
        if line.find('transmitted,') > -1:
            break   
    cols = line.split()
    colNum = 1
    for col in cols:
        if colNum==4:
            receviedValue = col
            break
        else:
            colNum += 1
    return int(receviedValue)


if __name__ == '__main__':
    pass