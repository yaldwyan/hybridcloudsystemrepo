'''
This module implements the low-level communication among different clouds to provide
cloud services e.g. VMs and Volumes.

It implements the required part for each cloud provider.
It can launch VMs from Nectar, Amazon, and Google Compute clouds.

A new cloud provider can be added to the hybrid cloud system, by simply 
implementing the required code for that cloud in the following methods of 
ResourcePool class:
1. launchVM method
2. terminateVM method
3. resizeVolume method
4. attachVolume method
5. detachVolume method

The last three methods are optional. They are useful if volumes will be used in
the new cloud.
 
Technically, you just add the following condition and then add your code at the
end of each method mentioned above:
    elif(self.vendor == cloudutils.YOUR_NEW_CLOUD):

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import os, socket, threading, time,logging
import boto.ec2
from googleapiclient.discovery import build 
import novaclient.exceptions
from oauth2client.client import GoogleCredentials
from novaclient.client import Client
import cinderclient.client as cc
import paramiko
from paramiko.ssh_exception import BadHostKeyException, AuthenticationException, SSHException
import amazon_utils, cloudutils, google_utils, nectar_utils

'''Generate a unique ID for each resource pool'''
_resourcePoolID = 0
resPoolLock = threading.Lock()

def generateResourcePoolId():
    with resPoolLock:
        global _resourcePoolID
        _resourcePoolID += 1
    return _resourcePoolID


class ResourcePool:
    '''
    This class communicates with all cloud providers to launch and terminate 
    virtual machines (VMs). Also, it creates, attaches and detaches volumes 
    to/from VMs in different clouds. 
    '''
    def __init__(self, resPoolId, resPoolName, resPoolType, numVms, volumeSize,
                 vendor):
        self.resPoolId = resPoolId
        self.resPoolType = resPoolType
        self.vendor = vendor
        self.resPoolName = resPoolName
        self.numVms = numVms
        self.numIdleVms = numVms
        self.vms = []
        self.volumeSize = volumeSize
        self.usedVolumeSize = 0
        generateTmpFolder()
        
    def getId(self):
        return self.resPoolId
    
    def isOverloaded(self):
        if self.numIdleVms == 0:
            return True
        return False
    
    def getNumOccupiedVms(self):
        return len(self.vms)
    
    def getNumIdleVms(self):
        return self.numIdleVms 
 
    def getNumVms(self):
        return self.numVms
    
    def decrementNumIdleVmsBy(self,decNum):
        self.numIdleVms -= decNum
    
    def incrementNumIdleVms(self, incNum):
        self.numIdleVms += incNum   
         
    def addVm(self, vm):
        self.vms.append(vm)

        
    def launchVM(self, vm, volumeSize=0, userData=None):
        '''
        Launch a Virtual Machine (VM)
        ''' 
              
        '''This is a default username for a VM. it can be changed like in the 
        case of Google Compute'''
        username = 'ubuntu'    
        
        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Launch a VM from Nectar cloud
            '''  
            try:
                startTime = time.time()
                credentials = nectar_utils.get_nova_credentials_v2()
                nova = Client(**credentials)
            
                image = nova.images.find(id=nectar_utils.UBUNTU_14_04_TRUSTY_IMAGE_ID)
                if vm.vmSize==cloudutils.VM_MEDUIM:
                    flavor = nova.flavors.find(name=nectar_utils.MEDUIME_SIZE)
                instance = nova.servers.create(name=vm.vmName, 
                                               image=image,
                                               flavor=flavor, 
                                               key_name=nectar_utils.NECTAR_KEY_PAIR_NAME,
                                               availability_zone=nectar_utils.AVAILABILITY_ZONE,
                                               userdata=userData)
                
                '''Poll at 3 second intervals, until the status is no longer 'BUILD' '''
                status = instance.status
                while status == 'BUILD':
                    time.sleep(3)
                    '''Retrieve the instance again so the status field gets update '''
                    instance = nova.servers.get(instance.id)
                    status = instance.status

                vm.setProviderVmId(instance.id)
                vm.setIp(nectar_utils.getIpAdd(instance))

                if volumeSize:
                    self.createAndAttachVolume(vm=vm, volumeSize=volumeSize)
                    
                endTime = time.time()
                execTime= endTime - startTime
               
                keyFname = nectar_utils.NECTAR_PRIVATE_KEY

                logging.info( "Nectar VM launched: vm id: {}, vm ip: {}, Time elapsed: {}".format( vm.provVmId, vm.ip, str(execTime)))
                print "A VM is launched at NeCtar Cloud"
            except:
                print "Error at ResourcePool.launchVM NeCtar cloud"
                
            finally:
                print("A NeCtar VM launch done.")
        
        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            '''
            Launch a VM from Amazon cloud
            '''
            startTime = time.time()
            conn = boto.ec2.connect_to_region(region_name= amazon_utils.AWS_REGION_NAME,  
                                       aws_access_key_id = amazon_utils.AWS_ACCESS_KEY_ID, 
                                       aws_secret_access_key = amazon_utils.AWS_SECRET_ACCESS_KEY)
             
            reservation = conn.run_instances(image_id=amazon_utils.AWS_UBUNTU_14_04_IMAGE_ID,
                                             key_name=amazon_utils.AWS_KEY_PAIR_NAME,
                                             security_groups=amazon_utils.AWS_SECURITY_GROUPS,
                                             instance_type=amazon_utils.AWS_LARGE_INSTANCE_TYPE)
            
            instance = reservation.instances[0]
            while instance.state != 'running':
                time.sleep(3)
                instance.update()
                       
            endTime = time.time()
            execTime = endTime - startTime
            
            logging.info("Amazon Instance launched: VM id: {}, state: {}, public IP:{}, time elapsed: {}"
                         .format(instance.id, instance.state,
                                 instance.public_dns_name, execTime))
            keyFname = amazon_utils.AWS_PRIVATE_KEY_NAME
                       
            vm.setProviderVmId(instance.id)    
            vm.setIp(instance.public_dns_name)
        
        elif(self.vendor == cloudutils.GOOGLE_CLOUD):
            '''
            Launch a VM from Google cloud
            '''
            startTime = time.time()
            
            vm.vmName = "testins"
            '''Credentials'''
            #credentials = GoogleCredentials.get_application_default()
            credentials = GoogleCredentials.from_stream('google/googlecreds.json')
            #print credentials.to_json()
            compute = build('compute','v1', credentials=credentials)
        
            config = {
                'name': vm.vmName,
                'machineType': google_utils.MACHINE_TYPE,
        
                # Specify the boot disk and the image to use as a source.
                'disks': [
                    {
                        'boot': True,
                        'autoDelete': True,
                        'initializeParams': {
                            'sourceImage': google_utils.SOURCE_DISK_IMAGE,
                        }
                    }
                ],
        
                # Specify a network interface with NAT to access the public
                # internet.
                'networkInterfaces': [{
                    'network': 'global/networks/default',
                    'accessConfigs': [
                        {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
                    ]
                }],
        
                # Allow the instance to access cloud storage and logging.
                'serviceAccounts': [{
                    'email': 'default',
                    'scopes': [
                        'https://www.googleapis.com/auth/devstorage.read_write',
                        'https://www.googleapis.com/auth/logging.write'
                    ]
                }]
        
             }
            project=google_utils.GOOGLE_PROJECT
            zone = google_utils.GOOGLE_ZONE
            operation =  compute.instances().insert(project=project,
                zone=zone, body=config).execute()
            
            print operation['name']
            
            google_utils.waitForOperation(compute, project, zone, operation['name'])
            
            print "end waiting method"
            
            '''Get IP of the VM'''
            result = compute.instances().list(project=project, zone=zone).execute()
            gInstances = result['items']
            ip = gInstances[0]['networkInterfaces'][0]['accessConfigs'][0]['natIP']
            
            endTime = time.time()
            execTime = endTime - startTime
            
            logging.info("Google Instance launched: VM id: {}, IP:{}, time elapsed: {}"
                         .format('ins2123', ip, execTime))
            
            keyFname = google_utils.GOOGLE_KEY
            
            vm.setProviderVmId(vm.vmName)    
            vm.setIp(ip)
            username = google_utils.USER_NAME
        
        '''
        Add your code here for a new cloud provider
        elif(self.vendor == cloudutils.YOUR_NEW_CLOUD):
        '''
            
        '''install software needed for cloud agent in the VM'''
        self.installSoftware(vm, keyFilename=keyFname, userName=username)
        vm.setStatus(cloudutils.RUNNING)
        self.addVm(vm)
    
    
    '''
    This method install software needed in the VM. It connects to the VM via
    ssh protocol.
    '''
    def installSoftware(self, vm, userName='ubuntu', keyFilename=None):
        
        if keyFilename==None:
            keyFullFilename = cloudutils.HYBRID_CLOUD_DIR + '/key/' + nectar_utils.NECTAR_PRIVATE_KEY
        else:
            keyFullFilename = cloudutils.HYBRID_CLOUD_DIR + '/key/' + keyFilename   
        
        print "Inside: installSoftware method."     
        time.sleep(15)

        hybridFolderName = "hybridcloud"
        
        isLb = False
        if vm.isLb:
            fileName = 'loadbalancer.py'
            isLb = True           
        else:
            fileName = "agent.py"
        #hybridCloudManagerDir = ''
        retries = 20
        nTry = 1
        for attempt in range(retries):
            try:
                print "Connecting to {} via SSH".format(vm.ip)
                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                
                client.connect(hostname=vm.ip,username=userName, 
                        key_filename=os.path.expanduser(keyFullFilename))
            
                print 'Executing pwd command.'
                stdin, stdout, stderr = client.exec_command('pwd')
                for line in stdout:
                    pwd = line.strip()
                    
                remoteHybridCloudDir = pwd + '/' + hybridFolderName
                print 'Executing mkdir ' + remoteHybridCloudDir
                stdin, stdout, stderr = client.exec_command('mkdir ' + 
                                                            remoteHybridCloudDir)
                for line in stdout:
                    print line
                      
                if isLb:
                    generateLoadbalancerFile(vm)
                else:
                    generateAgenFile(vm)
                    
                    
                localCwd = os.getcwd()               
                sftp = client.open_sftp()
                print 'Transferring data to ' + remoteHybridCloudDir
                #os.path.expanduser('~/Documents/LiClipse Workspace/testpro/agent.py')
                sftp.put(localCwd + '/tmp/' + vm.ip, 
                         remoteHybridCloudDir + '/' + fileName)
                
                if not isLb:
                    sftp.put(localCwd + '/www/index.html', 
                             remoteHybridCloudDir + '/index.html')
                    sftp.put(localCwd + '/webserver.py',
                             remoteHybridCloudDir + '/webserver.py')
                                
                                
                sftp.close()                 

                print 'Run agent code.'
                stdin, stdout, stderr = client.exec_command('cd ' + remoteHybridCloudDir + '; sudo nohup python ' + fileName + ' > /dev/null 2>&1 &')
                for line in stdout:
                    print line
                   
                client.close()
                
                if os.path.exists('tmp/' + vm.ip):
                    os.remove('tmp/' + vm.ip)
                break
            
            except (BadHostKeyException, AuthenticationException, 
                    SSHException, socket.error) as e: 
                print e               
                print "Not able to SSH to {}, number of tries={}".format(vm.ip, str(nTry))
                time.sleep(3) 
                nTry += 1
                                
        print "Installing SW on VM done."

        
    def terminateVM(self, delVmProvId): 
        '''
        Terminate a VM
        '''
        vm = self.getVmByProvVmID(delVmProvId)
        serverExists = False

        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Terminate a VM from Nectar cloud
            '''  
            try:
                startTime = time.time()
                
                credentials = nectar_utils.get_nova_credentials_v2()
                nova = Client(**credentials)
            
                servers = nova.servers.list()
                for s in servers:
                    if s.id == delVmProvId:
                        serverExists = True
                        break
                
                if serverExists:
                    logging.info('Deleting a server... (Nectar)')
                    nova.servers.delete(s)
                    logging.info('Server deleted.')
                else:
                    logging.info('Server does not exist (Nectar)')
                            
                endTime = time.time()
                execTime= endTime - startTime
                
                
                logging.info( "VM Deleted: {}, Time elapsed: {}".format( delVmProvId, str(execTime)))
                print "VM is deleted from NeCtar Cloud"
            except:
                print "Error at ResourcePool.terminateVM nectar cloud"
                
            finally:
                print("VM terminate done.")
    
        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            '''
            Terminate a VM from Google cloud
            '''
            conn = boto.ec2.connect_to_region(region_name= amazon_utils.AWS_REGION_NAME,
                                              aws_access_key_id = amazon_utils.AWS_ACCESS_KEY_ID,
                                              aws_secret_access_key = amazon_utils.AWS_SECRET_ACCESS_KEY) 

            conn.terminate_instances([delVmProvId])
        
        elif(self.vendor == cloudutils.GOOGLE_CLOUD):
            '''Credentials'''
            #credentials = GoogleCredentials.get_application_default()
            credentials = GoogleCredentials.from_stream('google/googlecreds.json')

            compute = build('compute','v1', credentials=credentials)
            compute.instances().delete(project=google_utils.GOOGLE_PROJECT, 
                                       zone=google_utils.GOOGLE_ZONE,
                                       instance=delVmProvId).execute()
        
        if vm in self.vms:
            self.vms.remove(vm)
        #self.incrementNumIdleVms(1)
        #print "Inside terminateVM(), number of idle VMs: {}".format(self.numIdleVms)

        
    def getVmByProvVmID(self, vmIdByProv):
        for vm in self.vms:
            if vm.provVmId == vmIdByProv:
                return vm
        return None
  
    
    def createAndAttachVolume(self, vm, volumeSize, device=None):
        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Create and attach a volume to a VM in NeCtar cloud
            '''  
            vol = Volume(volId=cloudutils.getUId(), size=volumeSize, vmId=vm.vmId)
            credentials = nectar_utils.get_nova_credentials_v2()
            nova = Client(**credentials)            
            cdr = cc.Client(**credentials)

            '''Create a volume'''
            try:
                newVolume = cdr.volumes.create(volumeSize, name="testvolume", availability_zone='melbourne-np')
                print "a volume is created."
            except novaclient.exceptions.OverLimit as ol:
                print ol
                print "Volume Storage Shortage"
            except:
                print "createAndAttachVolume: Error while creating volume"
                
            while newVolume.status != 'available':
                newVolume = nova.volumes.get(newVolume.id)
            
            vol.setProvVolumeId(newVolume.id)
            vm.setVolume(vol)
            '''attaching the volume to the VM'''
            att = nova.volumes.create_server_volume(vm.provVmId, newVolume.id, 
                                                    device='/dev/vdc')
            print "Attaching the volume to VM"
            while newVolume.status != 'in-use':
                newVolume = nova.volumes.get(newVolume.id)            

        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            pass
        elif(self.vendor == cloudutils.GOOGLE_CLOUD):
            pass
    
    def resizeVolume(self, vm, newSize):
        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Resize Volume in NeCtar cloud
            '''
            credentials = nectar_utils.get_nova_credentials_v2()   
            nova = Client(**credentials)
            
            serverId = vm.provVmId #'075f52ab-a1c5-463e-a9c7-ea4f07ca43c9'
            volumeId = vm.volume.provVolumeId #'0d72b6c8-e222-4f04-8af7-cd8678e31abc'
            st = time.time()
            '''Get a volume object first'''   
            vol = nova.volumes.get(volumeId)
            #print "volume is: {}, volume status: {}".format(vol.id, vol.status)
            
            '''Detaching the volume''' 
            print "Detaching the volume..."
            nova.volumes.delete_server_volume(serverId,
                                              volumeId)
            while vol.status != 'available':
                vol = nova.volumes.get(vol.id)
                            
            '''Extend the volume'''
            print "Extending the volume..."
            
            cdr = cc.Client(**credentials)        
            cVol = cdr.volumes.get(volumeId)
            cVol.extend(vol, newSize)
                   
            vol = nova.volumes.get(volumeId)    
            time.sleep(1)
            while vol.status != 'available':
                vol = nova.volumes.get(vol.id)
                #print "loop volume status: {}".format(vol.status)
        
            vol = nova.volumes.get(vol.id)
            #print "After exteding:\nvolume is: {}, volume status: {}, volume size: {}".format(vol.id, vol.status, vol.size)
            
            #Attach again
            print "Attaching the volume to VM again..."
            #attaching volume to vm
            attVol = nova.volumes.create_server_volume(serverId, vol.id)#, device='/dev/vdf')            
            while attVol.status != 'in-use':
                attVol = nova.volumes.get(attVol.id)          
            execTime = time.time() - st
            vm.volume.setSize(newSize)
                      
            print "Resize done:\nVolume id is: {}, volume status: {}, volume size: {}, execution time: {}".format(attVol.id, attVol.status, attVol.size, execTime)

                      
        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            pass
        elif(self.vendor == cloudutils.GOOGLE_CLOUD):
            pass
           
    
    def attachVolume(self, vmId, volumeId, volumSize):
        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Attach a volume to a VM in NeCtar cloud
            '''  
            pass
        
        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            pass
    
    def detachVolume(self, vm, volume): 
        if(self.vendor == cloudutils.NECTAR_CLOUD):
            '''
            Detach a volume from a VM in NeCtar cloud
            ''' 
            credentials = nectar_utils.get_nova_credentials_v2()
            nova = Client(**credentials)            
            
            nova.volumes.delete_server_volume(vm.provVmId, volume.provVolumeId) 
    
        elif(self.vendor == cloudutils.AMAZON_CLOUD):
            '''
            Detach a volume in Amazon cloud
            '''
            pass
        elif(self.vendor == cloudutils.GOOGLE_CLOUD):
            '''
            Detach a volume in Google cloud
            '''
            pass
    

'''
Generate agent code as a file to be transferred to back-end 
servers.
'''
def generateAgenFile(vm):
    
    try:
        templateAgentFile = open("cloudagent.py")       
        agentCodeStr = templateAgentFile.read()
        '''Inject cloud manger IP'''
        agentCodeStr = agentCodeStr.replace("${CLOUD_MANAGER_IP}", 
                                            cloudutils.cloudManagerIp)
        '''Inject VM's IP '''  
        agentCodeStr = agentCodeStr.replace("${VM_IP}", vm.ip)
        '''Inject VM's ID '''
        agentCodeStr = agentCodeStr.replace("${VM_ID}", vm.vmId)
        '''Inject server port'''
        agentCodeStr = agentCodeStr.replace('"${SERVER_PORT}"', str(vm.serverPort))
        
        templateAgentFile.close()
        filePath = 'tmp/' +  vm.ip
        tmpFile = open(filePath, 'w')
        tmpFile.write(agentCodeStr)
        tmpFile.close()
        
    except IOError as e:
        print "I/O error({}): {}".format(e.errno, e.strerror)
    

'''
Generate a load balancer code as a file to be transferred to a load balancer.
'''    
def generateLoadbalancerFile(vm):
    
    try:
        templateLBFile = open("multithreadedloadbalancer.py")       
        codeStr = templateLBFile.read()
        '''Inject cloud manger IP'''
        codeStr = codeStr.replace("${CLOUD_MANAGER_IP}", 
                                            cloudutils.cloudManagerIp)
        '''Inject VM's IP '''  
        codeStr = codeStr.replace("${LB_IP}", vm.ip)
        '''Inject VM's ID '''
        codeStr = codeStr.replace("${LB_ID}", vm.vmId)
        
        '''Inject ports for a load balancer '''
        codeStr = codeStr.replace('"${VM_PORT}"', str(vm.lb.vmPort) )
        codeStr = codeStr.replace('"${LB_PORT}"', str(vm.lb.lbPort) )
        
        templateLBFile.close()
    
        filePath = 'tmp/' +  vm.ip
        tmpFile = open(filePath, 'w')
        tmpFile.write(codeStr)
        tmpFile.close()
        
    except IOError as e:
        print "I/O error({}): {}".format(e.errno, e.strerror)    

def generateTmpFolder():
    tmpDir = 'tmp'
    if not os.path.exists(tmpDir):
        os.makedirs(tmpDir)


class VM:
    '''
    This class represents a virtual machine in the hybrid cloud system.
    '''
    def __init__(self, vmId, vmName, resPoolId, vmSize=cloudutils.VM_MEDUIM,
                 ip=None, availabilityZone=None, volume=None, 
                 isLb=False,lb=None, serverPort=None):
        self.vmId = vmId
        self.vmName = vmName
        self.vm_health = VmHealth(vm_id = self.vmId) 
        self.ip = ip
        self.status = cloudutils.IDLE
        self.vmHealth = VmHealth(vm_id=id)
        self.volume = volume
        self.vmSize = vmSize
        self.isLb = isLb
        self.lb = lb
        self.serverPort = serverPort
        self.resPoolId = resPoolId

    def setProviderVmId(self, provVmId):
        self.provVmId = provVmId   
         
    def setIp(self, ip):
        self.ip = ip   
        
    def update_vm_health(self, used_cpu, used_memory, used_harddisk):
        self.vm_health.used_cpu = used_cpu
        self.vm_health.used_memory = used_memory
        self.vm_health.used_harddisk = used_harddisk
        
    def setVolume(self, volume):
        self.volume = volume

    def getVolume(self):
        return self.volume
        
    def sendCommandMsgToVm(self, msg):
        if self.status == cloudutils.RUNNING:
            toVmSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            toVmSocket.connect((self.ip, cloudutils.VM_COMMAND_PORT))
            toVmSocket.sendall(msg)
            toVmSocket.close()
    def getId(self):
        return self.vmId
     
    def setStatus(self, s):
        self.status = s   
        
class VmHealth:
    '''
    This class represents a VM health information.
    '''
    def __init__(self, vm_id, cpuUtilization=0, memUtilization=0, usedVolume=0):
        self.vm_id = vm_id
        self.cpuUtilization = cpuUtilization
        self.memUtilization = memUtilization
        self.usedVolume = usedVolume
        
    
        
class Volume:
    '''
    This class represents a Volume information
    '''
    def __init__(self, volId, size, device=None, vmId=None):
        self.volId = volId
        self.size = size
        self.device = device
        self.attachedTo = vmId
    
    def setProvVolumeId(self, provVolId):
        self.provVolumeId = provVolId
         
    def getSize(self):
        return self.size
    
    def setSize(self, size):
        self.size = size
   
   
    
if __name__=="__main__":
    pass    

    
        
        