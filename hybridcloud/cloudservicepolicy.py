'''
This module is responsible for creating and initializing appropriate cloud 
services based on user requests.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import json, logging, socket, time
import cloudutils, message, provisioner


''' All autoscaling services for request are kept in this list'''
asServices = []


'''
This function handles user requests by initializing autoscaling service and 
load balancer if applicable and then ask the provisioner component to provision
VMs.
'''
def handleUserRequest(request):
    
    pId = cloudutils.getUId()
    pName = request.requestName + "_policy_" + pId 
    policy = Policy(policyId=pId, policyName=pName, 
                    highLoadThr=request.highLoadThr, 
                    lowLoadThr=request.lowLoadThr, volumeThr=request.volumeThr,
                    responseTimeThr=request.responseTimeThr)

    lb = None
    if request.hasLb:
        lbId = cloudutils.getUId()
        lbName = request.requestName + "_lb_" + lbId
        lb = LbService(lbId=lbId, lbName=lbName, 
                       lbPort=request.lbPort, vmPort=request.vmPort)
        
         
    asId = cloudutils.getUId() 
    aName = request.requestName + "_as_" + asId  
    asService = AsService(asId=asId, asName=aName, maxVms=request.maxVms, 
                          minVms=request.minVms, userData=request.userData,
                          lb=lb, policy=policy)
    
    asServices.append(asService)
    print "AS service added."
    
    provisioner.provision(request, asService)


def lookupAsServiceByAsId(asId):
    asService = None
    for asServ in asServices:
        if asServ.id == asId:
            asService = asServ
            break
    return asService

def lookupAsServiceByVmId(vmId):
    asService = None
    found = False
    for asServ in asServices:
        for vm in asServ.vms:
            if vm.vmId == vmId:
                asService = asServ
                found = True
                break
        if found:
            break
        
    return asService

class Policy():
    '''
    This class consists of  a set of thresholds and values that helps a cloud management 
    system take actions in order to handle potential failures properly during 
    a runtime of a job.
    '''
    def __init__(self, policyId, policyName,
                 highLoadThr=0.8, lowLoadThr=0.2,
                 volumeThr=0.8, responseTimeThr = 3):
        '''
        Constructor
        '''
        self.policyName = policyName
        self.policyId = policyId
        self.highLoadThr = highLoadThr
        self.lowLoadThr = lowLoadThr
        self.volumeThr = volumeThr
        self.responseTimeThr = responseTimeThr

    def getHighLoadThr(self):
        return self.highLoadThr
    
    def getLowLoadThr(self):
        return self.lowLoadThr
    
    def getVolumeThr(self):
        return self.volumeThr
    
    def getResponseTimeThr(self):
        return self.responseTimeThr
 
 

class AsService():
    '''
    This class implements an autoscaling service. This service applies cloud policies
    to change the capacity of the running VM instances or resize a volume. It also
    registers running VMs with a load balancer service.
    '''     
    
    def __init__(self, asId, asName, maxVms=2, minVms=1, 
                 userData=None,lb=None, policy=None):
        
        self.asId = asId
        self.asName = asName
        self.minVms = minVms
        self.maxVms= maxVms
        if lb:
            self.lb = lb
            self.hasLb = True
        else:
            self.lb = None
            self.hasLb = False
        self.policy = policy
        self.userData = userData
        self.vms = []
        self.status = cloudutils.HEALTHY
        
    def getStatus(self):
        return self.status
    
    def setStatus(self, status):
        self.status = status
    
    def integrateWithLb(self, lb):
        self.lb = lb
        self.hasLb = True
        
    def registerPolicy(self, policy):
        self.policy
    
    def addVm(self, vm):
        self.vms.append(vm)
        
    def removeVmByVmId(self, targetVmId):
        for vm in self.vms:
            if vm.id == targetVmId:
                self.vms.remove(vm)
                break
    def removeVM(self, targetVm):
        if targetVm in self.vms:
            self.vms.remove(targetVm)
    
    def getNumVms(self):
        return len(self.vms)

    def registerVMsWithLB(self):
        if self.lb.running:
            self.lb.vms = self.vms
            msg = {}
            msg[message.MESSAGE_TYPE] = message.REGISTER_VM_MSG
            beVms = []
            for vm in self.vms:
                vmItem = {}
                vmItem[message.ID] = vm.vmId
                vmItem[message.IP] = vm.ip                
                beVms.append(vmItem)
            msg[message.BACKEND_VMS] = beVms
            for be in beVms:
                print be
            jsonMsg = json.dumps(msg)
            
            self.lb.sendCommandMsgToLB(jsonMsg)
                  
    def getVmObj(self, vmId):
        for vm in self.vms:
            if vm.getId() == vmId:
                return vm
        return None  

class LbService():
    '''
    This class implements a load balancer service that is needed to balance load 
    among back-end VMs.
    '''
    def __init__(self, lbId, lbName,lbPort, vmPort,
                 lbVm=None, ip=None, vms=None):
        
        self.lbName = lbName
        self.lbId = lbId
        if ip:
            self.ip = ip
            self.running = True
        else:
            self.running = False
        self.lbPort = lbPort
        self.vmPort = vmPort
        if vms:
            self.vms = vms
        else:
            self.vms = []        
        self.lbVm = lbVm    
    
    def setLbRunningStatus(self, status):
        self.running = status
    
    def setLbIP(self, ip):
        self.ip = ip
        self.running = True
          
    def isLbRunning(self):
        return self.running
    
    def sendCommandMsgToLB(self, msg):
        tries = 0
        while tries < 5:
            try:
                tries += 1
                lbSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                lbSocket.connect((self.ip, cloudutils.LB_COMMAND_PORT))
                lbSocket.sendall(msg)
                lbSocket.close()
                break
            except socket.error, e:
                if 'Connection refused' in e:
                    print 'Connection to LB refused, Number of attempts: {}'.format(tries)
                time.sleep(2)
                    
            finally:
                lbSocket.close()
        
    def setVMRunningLb(self, lbVm):
        self.lbVm = lbVm
        self.running = True 
           
    ''' This function sends a message to the vm running the load balancer to 
    to register a vm as back-end vm.
    '''    
    def registerVM(self, vm):        
        if vm not in self.vms:
            self.vms.append(vm)           
        if self.running:
            msg = {}
            msg[message.MESSAGE_TYPE] = message.REGISTER_VM_MSG            
            beVms = []            
            vmItem = {}
            vmItem[message.ID] = vm.vmId
            vmItem[message.IP] = vm.ip                
            beVms.append(vmItem)            
            msg[message.BACKEND_VMS] = beVms           
            registerJMsg = json.dumps(msg)
            '''send registerVM message to the VM running LB'''
            self.sendCommandMsgToLB(registerJMsg)
            logging.info("New VM, name:{}, ip:{}, is registered with LB".format(vm.vmName, vm.ip))
            print "New VM, name:{}, ip:{}, is registered with LB".format(vm.vmName, vm.ip)
                  
    ''' 
    This function sends a message to the vm running the load balancer to 
    unregister a vm from its back-end vm list.
    '''                            
    def unregisterVM(self, failedVm):
        
        #if vm not in self.vms:
        #    self.vms.append(vm)
        print "unregistering vm, ip: {}".format(failedVm.ip)
        if self.running:
            msg = {}
            msg[message.MESSAGE_TYPE] = message.UNREGISTER_VM_MSG
            
            msg[message.ID] = failedVm.vmId
            msg[message.IP] = failedVm.ip               
            
            
            unRegisterJMsg = json.dumps(msg)
            #send registerVM message to the VM running LB
            self.sendCommandMsgToLB(unRegisterJMsg)
            
            try:
                self.vms.remove(failedVm)
            except ValueError:
                logging.info("LbService.unregisterVM(): Failed vm is not in loadbalancer's lists of vms.")
                print "LbService.unregisterVM(): Failed vm is not in loadbalancer's lists of vms."
                