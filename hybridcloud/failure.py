'''
This module contains all details about failures that is dealth in the proposed 
hybrid cloud system.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import time

'''Types of failures'''
VM_CRASH_FAILURE = 'vmcrashfailure'
VM_SLOWDOWN_FAILURE = 'vmslowdownfailure'
VM_HIGHLOAD_FAILURE = 'vmhighloadfailure'
VM_LOWLOAD_FAILURE = 'vmlowloadfailure'
VM_STORAGE_SHORTAGE_FAILURE = 'storageshortagefailure'


class Failure():
    '''
    This class consists of all information about each failure occurs in the 
    hybrid cloud system.
    '''
    def __init__(self, failureType, asService, failedVm=None):
        self.failureType = failureType
        self.asService = asService
        self.failedVm = failedVm
        self.timestampeStartRecoveryTime()
 
    def timestampeStartRecoveryTime(self):
        self.startRecoveryTime = time.time()   
         
    def timestampeEndRecoveryTime(self):
        self.endRecoveryTime = time.time()
    
    def getTimeToRecovery(self):
        return self.endRecoveryTime - self.startRecoveryTime
                
        
if __name__ == '__main__':
    pass