'''
This module runs at a VM running a load balancing service. It periodically sends a 
health message to the cloud manager. It also runs a cloud application in another
thread. In our case the cloud application is a web server in webserver module.
It also listens to commands from the cloud manager  

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import SocketServer, json, logging, os, select, socket, threading, time


buffer_size = 4096
delay = 0.0001

'''Message types and keys'''
MESSAGE_TYPE = 'messagetype'
REGISTER_VM_MSG = 'registervmmsg'
UNREGISTER_VM_MSG = 'unregistervmmsg'
BACKEND_VMS = 'backendvms'
ID = 'id'
IP = 'ip'
TEST_MSG = 'testmsg'

'''Cloud manager configuration'''
CLOUD_MANAGER_IP = "${CLOUD_MANAGER_IP}"
MONITORING_PORT = 9034
LB_COMMAND_PORT = 8943

'''Load balancer ID and IP. They are assigned by the cloud manager '''
lbId = "${LB_ID}"
lbIp = "${LB_IP}"

'''
Ports needed to balance load. 
lbPort is a port on which the load balancer listens to receive http requests 
coming from users while 
vmPort is a port to which the load balancer forwards http requests
'''
vmPort = "${VM_PORT}"
lbPort = "${LB_PORT}" 

'''
A list of all VMs running as back-end servers
'''
backendVms = []
_beIndex = 0
_BackenVmLock = threading.Lock()



class Forwarder:
    '''
    This class creates a connection to a back-end server. 
    '''
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def forwardTo(self, backendVm):
        try:
            self.forward.connect((backendVm.host, backendVm.port))
            return self.forward
        except Exception, e:
            print e
            return False


class Loadbalancer:
    '''
    This class loads balances among registered back-end servers.
    '''
    input_list = []
    channel = {}

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.setblocking(0)
        self.server.bind((host, port))
        self.server.listen(200)

    def balance(self):
        self.input_list.append(self.server)
        while 1:
            time.sleep(delay)
            ss = select.select
            
            inputready, outputready, exceptready = ss(self.input_list, [], [])
            for self.s in inputready:
                if self.s == self.server:
                    self.onAccept()
                    break
                else:
                    self.data = self.s.recv(buffer_size)
                    if len(self.data) == 0:
                        self.onClose()
                        break
                    else:
                        self.onRecv()


    def onAccept(self):
        backendVm = nextBackendVm()
        backendVm.incrementReqCount()
        forward = Forwarder().forwardTo(backendVm)
        clientsock, clientaddr = self.server.accept()
        if forward:
            self.input_list.append(clientsock)
            self.input_list.append(forward)
            self.channel[clientsock] = forward
            self.channel[forward] = clientsock
        else:
            print "Can't establish a connection with a back-end server.",
            print "Closing connection with user", clientaddr
            clientsock.close()


    def onClose(self):
        '''remove both sockts from input_list'''
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        '''close the connection with user'''
        self.channel[out].close()  
        '''close the connection with a back-end server'''
        self.channel[self.s].close()
        '''delete both sockets'''
        del self.channel[out]
        del self.channel[self.s]

    def onRecv(self):
        data = self.data
        self.channel[self.s].send(data)



'''
This function is used to select back-end VMs evenly
'''
def nextBackendVm():
    with _BackenVmLock:
        global _beIndex
        if len(backendVms)==1 or _beIndex == len(backendVms):
            _beIndex = 0
        currBackendVm = backendVms[_beIndex]
        _beIndex += 1
        return currBackendVm
    
class BackendVm():
    '''
    This class represents a back-end server running on a separate VM.
    '''
    def __init__(self, beID, ip, port=None):
        self.id = beID
        self.ip = ip
        
        if port:
            self.port = port
        
        self.reqCount = 0
        self.reqCountLock = threading.Lock()
        
    def incrementReqCount(self):
        with self.reqCountLock:
            self.reqCount += 1
        incrementReqCount()

    def getReqCount(self):
        return self.reqCount


'''
Register a back-end VM with LB
'''    
def registerBackendVm(newBeVm):
    with _BackenVmLock:
        if newBeVm not in backendVms:
            backendVms.append(newBeVm)

'''
Unregister a back-end VM from LB by VM' ID.
'''          
def unregisterBackendVmById(vmId):
    with _BackenVmLock:
        for beVm in backendVms:
            if beVm.id == vmId:
                backendVms.remove(beVm)
                break

'''
Unregister a back-end VM from LB by a VM object.
'''             
def unregisterBackendVm(failedBeVm):
    with _BackenVmLock:
        if failedBeVm in backendVms:
            backendVms.remove(failedBeVm)

'''
This function sends a message to the cloud manager.
'''   
def sendMsgToCloudManager(msg):
    cmSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cmSocket.connect((CLOUD_MANAGER_IP, MONITORING_PORT))
    cmSocket.sendall(msg)
    cmSocket.close()
                

class CommandHandler(SocketServer.BaseRequestHandler):
    '''
    This class handles a command message coming from the cloud manager.
    '''
    def handle(self):
        global vmPort
        self.data = self.request.recv(1024).strip()
        msg = json.loads(self.data)
        logging.info("command handler received: {}".format( self.data))
        if msg[MESSAGE_TYPE] == REGISTER_VM_MSG:
            logging.info("Received command from CSM registervms: {}".format(msg))
            beVms = msg[BACKEND_VMS]
            if len(beVms) > 0:
                for vm in beVms:
                    newBeVm = BackendVm(vm[ID], vm[IP], vmPort)
                    registerBackendVm(newBeVm)

            global backendVms
            logging.info("Number of back-end servers now: {}".format(str(len(backendVms)))) 
            for be in backendVms:
                logging.info("Back-end IP: {}".format(be.ip))
        
        elif msg[MESSAGE_TYPE] == UNREGISTER_VM_MSG:
            fVmId = msg[ID]
            unregisterBackendVmById(fVmId)
        
        elif msg[MESSAGE_TYPE] == TEST_MSG:
            '''global isTest, cpuTest, memTest, volumeTest, testCpuUtilization
            global testMemoryUtilization, testUsedVolumeSpace
            isTest = True
            if CPU in jsonMsg:
                testCpuUtilization = jsonMsg[CPU]
                cpuTest = True
            if MEMORY in jsonMsg:
                testMemoryUtilization = jsonMsg[MEMORY]
                memTest = True
            if VOLUME in jsonMsg:
                testUsedVolumeSpace = jsonMsg[VOLUME]
                volumeTest = True'''
            testThr = threading.Thread(target=countReqForPeriod)
            testThr.start()
            self.request.sendall("OK\n")




isLbStarted = False

'''
The following functions and variables to count requests.
'''
_requestCount = 0
_reqCountLock = threading.Lock()

def incrementReqCount():
    with _reqCountLock:
        global _requestCount
        _requestCount += 1

def getReqCount():
    global _requestCount
    return _requestCount

def getTotalRequestCount():
    total = 0
    for be in backendVms:
        total += be.getReqCount()

def countReqForPeriod():
    testcounterFile = open("testcounter",'w')
    logging.info("Test is running now.")
    period = 60
    endTest = time.time() + period
    secCount = 0
    previousReqCount = getReqCount()
    while endTest > time.time():
        time.sleep(0.5)
        secCount += 1
        currentReqCount = getReqCount()
        reqCount = currentReqCount - previousReqCount
        previousReqCount = currentReqCount
        testcounterFile.write( str(secCount) + ', ' + str(reqCount) + '\n')
    testcounterFile.close()

    
    
if __name__ == '__main__':
    path = os.path.expanduser('~/hybridcloud')
    logFilename = os.path.join(path, "loadbalancer.log")
    logging.basicConfig(filename=logFilename, level=logging.INFO,
                        format='%(asctime)s %(message)s')
    
    logging.info("Load balancer is starting...")
    logging.info('Command listener at load balancer is starting...')
    address = ('', LB_COMMAND_PORT)
    cmdServer = SocketServer.TCPServer(address,CommandHandler)
    cmdThread = threading.Thread(target=cmdServer.serve_forever)
    cmdThread.start()
    
    loadbalancer = Loadbalancer('', lbPort)
    lbThr = threading.Thread(target=loadbalancer.balance)
    lbThr.start()
    global isLbStarted
    isLbStarted = True   
    logging.info("Load balancer serving at IP: {}, port:{}".format(lbIp, lbPort))

    lbThr.join()
    cmdThread.join()    
