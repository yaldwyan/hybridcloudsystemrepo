'''
This module is used for testing the hybrid cloud management system. It generates
failures on purpose to see how the system detects and recovers from failure.

To run an experiment, simply run this module and pass test type and the ip 
address of a VM, which will generate a failure intentionally, as arguments.

Test types are slowdowntest, highloadtest, lowloadtest and fullvolumetest.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import cloudutils, message
import json, socket, sys

def testSlowdownFailure(ip):
    msg = {}
    msg[message.MESSAGE_TYPE] = message.SERVER_SLEEP_TIME
    msg[message.SERVER_SLEEP_VALUE] = 7
    sendTestCommandToAgent(message.SERVER_SLEEP_TIME, ip, json.dumps(msg))


def testHighLoadFailure(ip):
    msg = {}
    msg[message.MESSAGE_TYPE] = message.TEST_MSG
    msg[message.CPU] = 0.85
    sendTestCommandToAgent(message.TEST_MSG, ip, json.dumps(msg))

def testLowLoadFailure(ip):
    msg = {}
    msg[message.MESSAGE_TYPE] = message.TEST_MSG
    msg[message.CPU] = 0.15
    sendTestCommandToAgent(message.TEST_MSG, ip, json.dumps(msg))

def testFullVolumeFailure(ip):
    msg = {}
    msg[message.MESSAGE_TYPE] = message.TEST_MSG
    msg[message.VOLUME] = 0.85
    sendTestCommandToAgent(message.SERVER_SLEEP_TIME, ip, json.dumps(msg))
    
def sendTestCommandToAgent(msgType, ip, msg):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   
    sock.connect((ip, cloudutils.VM_COMMAND_PORT))
    try:
            sock.sendall(msg)
    finally:
            sock.close()
    print "Test command sent. Message type: {}".format( msgType )


if __name__ == '__main__':
    if len( sys.argv ) > 1:
        testType = sys.argv[1]
        ip = sys.argv[2]
        '''
        Note that if you want to test a crash failure, you just need to terminate 
        the target virtual machine manually, i.e, via the web browser or CLI'''
        if testType == 'slowdowntest':
            testSlowdownFailure(ip)
        elif testType == 'highloadtest':
            testHighLoadFailure(ip)
        elif testType == 'lowloadtest':
            testLowLoadFailure(ip)
        elif testType == 'fullvolumetest':
            testFullVolumeFailure(ip)
        else:
            print "unknown test type."  
    else:
        print "Input argument is incorrect. Try again."
        