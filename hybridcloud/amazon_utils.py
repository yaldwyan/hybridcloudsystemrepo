'''
This file contains all configurations needed to connect to Amazon Cloud.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

AWS_ACCESS_KEY_ID = 'YOUR_AWS_ACCESS_KEY_ID'
AWS_SECRET_ACCESS_KEY = 'YOUR_AWS_SECRET_ACCESS_KEY'
AWS_REGION_NAME = 'ap-southeast-2'

AWS_PRIVATE_KEY_NAME = 'YOUR_AWS_PRIVATE_KEY_NAME'
AWS_KEY_PAIR_NAME='YOUR_AWS_KEY_PAIR_NAME'

AWS_SECURITY_GROUPS = ['YOUR_SECURITY_GROOP']
AWS_LARGE_INSTANCE_TYPE = 't2.large'

AWS_UBUNTU_14_04_IMAGE_ID = 'ami-6c14310f'