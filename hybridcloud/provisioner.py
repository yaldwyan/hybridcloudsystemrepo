'''
This module deploys a hybrid cloud manager system. It initializes resource pool 
objects which launch and terminate virtual machines (VMs) and create, attach and
detach volumes to VMs in different clouds. 

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''
import logging
import time, threading

import cloudutils
from resourcepool import VM

'''Private resource pool from a private cloud'''
privResPool = None

'''A list of public resource pools from different clouds'''
pubResPools = []

def setPrivateResourcePool(privRP):
    global privResPool
    privResPool = privRP

'''
This function adds a public resource pool to the list
'''    
def addPublicResourcePool(pubRP):
    global pubResPools
    pubResPools.append(pubRP)

    
'''Get the total number of VMs in the hybrid cloud system'''
def getTotalHybridCloudNumVms():
    global privResPool, pubResPools
    total = 0
    for pubRP in pubResPools:
        total += pubRP.getNumVms() 
    total += privResPool.getNumVms()
    return  total


'''Get the total number of idle VMs in the hybrid cloud system'''
def getTotalHybridCloudIdelNumVms():
    global privResPool, pubResPools
    total = 0
    for pubRP in pubResPools:
        total += pubRP.getNumIdleVms()        
    total += privResPool.getNumIdleVms()
    return  total


'''Get the total number of idle public VMs from different cloud providers in the
hybrid cloud system'''
def getTotalPublicResPoolsIdleNumVms():
    global pubResPools
    total = 0
    for pubRP in pubResPools:
        total += pubRP.getNumIdleVms()        
    return  total

'''Get the total number of public VMs from different cloud providers in the
hybrid cloud system'''
def getTotalPublicResPoolsNumVms():
    global pubResPools
    total = 0
    for pubRP in pubResPools:
        total += pubRP.getNumVms()        
    return  total    
    
'''Check for availability of resources'''
def checkForVmAvailability():
    if getTotalHybridCloudIdelNumVms() == 0:
        return False
    return True

'''Get a resource pool object based on its ID'''
def getResourcePool(resPoolId):
    global privResPool, pubResPools
    if privResPool.getId() == resPoolId:
        rp = privResPool
    else:
        for pubRP in pubResPools:
            if pubRP.getId() == resPoolId:
                rp = pubRP
    return rp

'''
This function provisions resources based on a user request. It checks resource
availability. If there is enough resources, it create a provision request handler
thread and pass number of resources needed in user requests. Otherwise, it rejects
the user requests.
'''
provisionLocker = threading.Lock()
def provision( userRequest, asService):
    global privResPool, pubResPools
    reqVmNum = userRequest.numVms
    if userRequest.hasLb:
        reqVmNum += 1
    with provisionLocker:
        if getTotalHybridCloudIdelNumVms() >= reqVmNum:             
            privVmNum = 0
            totalPubVmNum = 0
            if privResPool.getNumIdleVms() > 0:
                if privResPool.getNumIdleVms() >= reqVmNum:
                    privVmNum = reqVmNum   
                else:
                    privVmNum = privResPool.getNumIdleVms()
                    totalPubVmNum = reqVmNum - privVmNum
            else:
                totalPubVmNum = reqVmNum
            privResPool.decrementNumIdleVmsBy(privVmNum)     
            '''Choose public resources from public resource list available'''       
            tmpPubVmNums = totalPubVmNum
            if totalPubVmNum > 0:
                pubReqResList = []
                for pubRP in pubResPools:
                    currentPubRPIdleVmNum = pubRP.getNumIdleVms()
                    if currentPubRPIdleVmNum > 0:
                        if currentPubRPIdleVmNum >= tmpPubVmNums:
                            pubRP.decrementNumIdleVmsBy(tmpPubVmNums)
                            pubReqResList.append((pubRP,tmpPubVmNums))
                            break
                        else:
                            pubRP.decrementNumIdleVmsBy(currentPubRPIdleVmNum)
                            pubReqResList.append((pubRP,currentPubRPIdleVmNum))
                            tmpPubVmNums -= currentPubRPIdleVmNum            
            prht = ProvisionRequestHandlerThread(userRequest, asService,
                                                privVmNum, 
                                                totalPubVmNum, pubReqResList)
            prht.start()
        else:
            print "User request is rejected. " 



class ProvisionRequestHandlerThread(threading.Thread):
    '''
    This class handles a user request for cloud services e.g. VMs. It create a
    separate thread for each VM launch.
    '''
    def __init__(self, userRequest, asService, privVmNum, 
                 totalPubVmNum, pubReqResList):
        threading.Thread.__init__(self)
        self.userRequest = userRequest
        self.asService = asService
        self.privVmNum = privVmNum
        self.totalPubVmNum = totalPubVmNum
        self.pubReqResList = pubReqResList
    
    def run(self):
        global privResPool
        st = time.time()
        launcherThreadList = []
        
        volumeSize = 0
        if self.userRequest.hasVolume:
            volumeSize = self.userRequest.volumeSize
        
        '''check for a load balancer'''
        if self.asService.hasLb:
            if self.privVmNum > 0:
                '''launch a load balancer in a private cloud'''
                priVmLauncherThr = VMLauncherThread(privResPool,
                                                    self.asService, isLb=True)
                launcherThreadList.append(priVmLauncherThr)
                priVmLauncherThr.start()
                self.privVmNum -= 1
                
            else:
                '''launch a load balancer in a public cloud'''
                pubRP, vmNum = self.pubReqResList.pop(0)
                vmNum -= 1
                if vmNum > 0:
                    self.pubReqResList.insert(0,(pubRP,vmNum))          
                puVmLauncherThr = VMLauncherThread(pubRP,
                                                   self.asService, isLb=True)
                launcherThreadList.append(puVmLauncherThr)
                puVmLauncherThr.start()
                self.totalPubVmNum -= 1
                
        '''Launch VMs in the private cloud'''                    
        if self.privVmNum > 0:
            for _ in range(self.privVmNum):   
                priVmLauncherThr = VMLauncherThread(privResPool, self.asService,
                                                    volumSize=volumeSize)
                launcherThreadList.append(priVmLauncherThr)
                priVmLauncherThr.start()     
                               
        '''Launch VMs in the public clouds'''
        if self.totalPubVmNum > 0:
            for pubRPTuple in self.pubReqResList:
                pubRP, vmNum = pubRPTuple
                for _ in range(vmNum):   
                    puVmLauncherThr = VMLauncherThread(pubRP, self.asService,
                                                   volumSize=volumeSize)
                launcherThreadList.append(puVmLauncherThr)
                puVmLauncherThr.start()
                
        for t in launcherThreadList:
            t.join()
        et = time.time()   
        print "All instances are running for request name: {}, time elapsed: {}".format(self.userRequest.requestName, str(et-st))
        logging.info("All instances are running for request name: {}, time elapsed: {}".format(self.userRequest.requestName, str(et-st)))
        logging.info("Number of idle private resources is: {}".format(str(privResPool.getNumIdleVms())))
        logging.info("Number of idle public resources in all public clouds is: {}".format(str(getTotalPublicResPoolsIdleNumVms)))
        
        '''Register VMs with the load balancer'''
        if self.asService.hasLb:
            print "provisioner registers VMs with Lb"
            self.asService.registerVMsWithLB()
        
    

class VMLauncherThread(threading.Thread):
    '''
    This class launch a VM in a separate thread.
    '''
    def __init__(self, resPool, asService, isLb=False, volumSize=0):
        threading.Thread.__init__(self)
        self.resPool = resPool
        self.asService = asService
        self.isLb = isLb
        self.volumeSize = volumSize
    
    def run(self):
        vmId = cloudutils.getUId()
        vmName = self.asService.asName + "_ins_" + vmId
        if self.isLb:
            vm = VM(vmId, vmName, resPoolId=self.resPool.resPoolId,
                    isLb=self.isLb, lb=self.asService.lb)
        else:
            vm = VM(vmId, vmName, resPoolId=self.resPool.resPoolId)
            if self.asService.hasLb:
                vm.serverPort = self.asService.lb.vmPort      
        self.resPool.launchVM(vm, volumeSize=self.volumeSize)
       
        if self.isLb:
            print "VM is a load balancer, IP: {}".format(vm.ip)
            self.asService.lb.setVMRunningLb(vm)
            self.asService.lb.setLbIP(vm.ip)
        else:
            print "VM is a back-end server, IP: {}".format(vm.ip)
            self.asService.addVm(vm)
        
        print "VM launcher completed job with VM, its name is " + vm.vmName
        
        
'''
This function is used to generate a cloud agent code after assigning the IP of
the hybrid cloud manager
'''            
def generateAgentCode(): 
    agentFile = open("cloudagent.py")       
    agentCodeStr = ""
    agentCodeStr = agentFile.read()
    return agentCodeStr.replace("${CLOUD_MANAGER_IP}", 
                                cloudutils.cloudManagerIp)

def generateUserdata():
    agentCodeStr = generateAgentCode()    
    userDataStr = ""
    userDataFile = open("userdata.py")
    userDataStr = userDataFile.read()
    return userDataStr.replace('${AGENT_CODE}', agentCodeStr)

'''
This function resizes the volume for a specific VM.
'''  
def resizeVolume(vm, newSize):
    rPool = getResourcePool(vm.resPoolId)
    rPool.resizeVolume(vm, newSize)

'''
This function terminates a VM and remove it from autoscaling service..
'''  
def releaseVM(vm, asService):
    rPool = getResourcePool(vm.resPoolId)
    rPool.terminateVM(vm.provVmId)  
    asService.removeVM(vm) 
    
'''

'''
def terminateFailedVmAndLaunchNewVm(failedVm, asService):
    print "Terminating a failed Vm and launching a new VM. Its IP: {}".format(failedVm.ip)
    '''Terminating failed VM'''
    releaseVmThr = threading.Thread(target=releaseVM, args=(failedVm, asService,))
    releaseVmThr.start() 
    '''Decrease it since vm is crashed (test only)'''
    #privResPool.numIdleVms -= 1   
    '''Launching a new VM '''
    newVm = launchNewVm(asService)
    return newVm
    
    
def launchNewVm(asService):
    '''Launching a new VM '''
    global privResPool, pubResPools
    rPool =None
    with provisionLocker:
        print "LaunchNewVm: Number of idle vm private: {}".format( privResPool.getNumIdleVms())
        if privResPool.getNumIdleVms() >= 1:
            rPool = privResPool
            privResPool.decrementNumIdleVmsBy(1)
        else:
            for pubRP in pubResPools:
                if pubRP.getNumIdleVms() >= 1:
                    rPool = pubRP
                    pubRP.decrementNumIdleVmsBy(1)
                    break
                         
    vmId = cloudutils.getUId()
    vmName = asService.asName + "_ins_" + vmId    
    newVm = VM(vmId, vmName, resPoolId=rPool.resPoolId)
    if asService.hasLb:
        newVm.serverPort = asService.lb.vmPort       
    rPool.launchVM(newVm)   
    asService.addVm(newVm) 
    return newVm
    
   
    
if __name__ == '__main__':
    pass
