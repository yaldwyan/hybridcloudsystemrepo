'''
This module runs at a VM running a back-end server. It periodically sends a 
health message to the cloud manager. It also runs a cloud application in another
thread. In our case the cloud application is a web server in webserver module.
It also listens to commands from the cloud manager  

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import SocketServer, json, logging, os, socket, subprocess, threading, time
import webserver

''' Running vm identity
The vm ip and id is assign by the cloud manager during the VM launch process '''
vmIp = "${VM_IP}"
vmId = "${VM_ID}"
''' A port on which the web server listens to http requests coming from a
load balancer '''
serverPort = "${SERVER_PORT}"
''' A port on which the cloud agent listens to commands from the cloud
manager''' 
VM_COMMAND_PORT = 8945

''' Cloud manager IP'''
CLOUD_MANAGER_IP = "${CLOUD_MANAGER_IP}"

''' A port on which the cloud manager listens to health messages from the cloud
 agent'''
MONITORING_PORT = 9034

'''Message types and keys'''
MESSAGE_TYPE = 'messagetype'
VM_HEALTH_MSG = 'vmhealthmsg'
TEST_MSG = 'testmsg'
CPU_UTILIZATION = "cpuutilization"
MEMORY_UTILIZATION = "memoryutilization"
USED_VOLUME_SPACE = "usedvolumespace"
ID = 'id'
IP = 'ip'
SERVER_SLEEP_VALUE = 'serversleepvalue' 
CPU = 'cpu'
MEMORY = 'memory'
VOLUME = 'volume'
SERVER_SLEEP_TIME = 'serversleeptime'

targetDevice = "sda2"

'''Variables needed for test scenarios'''
isTest = False
countTest = 0
cpuTest = False
memTest = False
volumeTest = False
testCpuUtilization = 0
testMemoryUtilization = 0
testUsedVolumeSpace = 0
        

''' Helper functions'''
                    
def toJsonHealthMsg():
    global isTest
    msg = {}
    msg[MESSAGE_TYPE] = VM_HEALTH_MSG
    msg[ID] = vmId

    if not isTest:
        msg[CPU_UTILIZATION] = getCpuUtilization()
        msg[MEMORY_UTILIZATION] = getMemoryUtilization()
        msg[USED_VOLUME_SPACE] = getUsedVolumeSpace()
    else:
        print "cputest" + str(cpuTest)

        if cpuTest:
            msg[CPU_UTILIZATION] = testCpuUtilization
        else:
            msg[CPU_UTILIZATION] = getCpuUtilization()
            
        if memTest:
            msg[MEMORY_UTILIZATION] = testMemoryUtilization
        else:
            msg[MEMORY_UTILIZATION] = getMemoryUtilization()
        
        if volumeTest:
            msg[USED_VOLUME_SPACE] = testUsedVolumeSpace
        else:
            msg[USED_VOLUME_SPACE] = getUsedVolumeSpace()
    
    
    return json.dumps(msg)
    

def getUsedVolumeSize():
    global targetDevice   
    cmd = "df"    
    outputdata, errordata = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE, 
                                            shell=True).communicate()    
    lines = outputdata.splitlines()
    for line in lines:
        if line.find(targetDevice) > -1:
            break 
    cols = line.split()
    colNum = 1
    for col in cols:
        if colNum==5:
            useValue = col
            break
        else:
            colNum += 1   
    return useValue.split('%')[0]

'''The following functions can be done the same as getUsedVolumeSize(), by using
suprocess.Popen and passing the appropriate command and then taking out the 
value needed from the output.'''
def getCpuUtilization():
    return 0.5

def getMemoryUtilization():
    return 0.5

def getUsedVolumeSpace():
    return 0.6      


class HealthMsgSenderThread(threading.Thread):
    '''
    This class is responsible for sending health messages to the cloud manager 
    periodically. It has to be run in a seperate thread. 
    '''    
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        logging.info("Health message Sender thread is starting...")
        n=3
        global countTest, isTest, cpuTest, memTest, volumeTest
        while True:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   
            sock.connect((CLOUD_MANAGER_IP, MONITORING_PORT))
            jsonHealthMsg = toJsonHealthMsg()
            if n==3:
                logging.info(vmId + " : " + jsonHealthMsg)
                n=1
            try:
                    
                sock.sendall(jsonHealthMsg)
            finally:
                sock.close()
            
            if isTest:
                isTest = False
                isTest = False
                cpuTest = False
                memTest = False
                volumeTest = False                
            n += 1
            time.sleep(4)
 
 
class CommandHandler(SocketServer.BaseRequestHandler):
    '''
    This class handles command messages coming from the cloud manager.
    '''
    def handle(self):
        self.data = self.request.recv(1024).strip()
        jsonMsg = json.loads(self.data)
        print "command handler received: " + self.data
        if jsonMsg[MESSAGE_TYPE] == TEST_MSG:
            global isTest, cpuTest, memTest, volumeTest, testCpuUtilization
            global testMemoryUtilization, testUsedVolumeSpace
            isTest = True
            if CPU in jsonMsg:
                testCpuUtilization = jsonMsg[CPU]
                cpuTest = True
            if MEMORY in jsonMsg:
                testMemoryUtilization = jsonMsg[MEMORY]
                memTest = True
            if VOLUME in jsonMsg:
                testUsedVolumeSpace = jsonMsg[VOLUME]
                volumeTest = True
        elif jsonMsg[MESSAGE_TYPE] == SERVER_SLEEP_TIME:
            webserver.setSleepTime(jsonMsg[SERVER_SLEEP_VALUE])
            

if __name__ == '__main__':
    '''Create a log file'''
    path = os.path.expanduser('~/hybridcloud')
    logFilename = os.path.join(path, "agent.log")
    logging.basicConfig(filename=logFilename, level=logging.INFO,
                        format='%(asctime)s %(message)s')  
    '''Run health message sender thread'''
    healthMsgSender = HealthMsgSenderThread()
    healthMsgSender.start()
    logging.info('Health message Sender thread is running now ...')
    
    '''Start listening to commands from the cloud manager'''
    logging.info('Command listener is starting...')
    address = ('', VM_COMMAND_PORT)
    cmdServer = SocketServer.TCPServer(address,CommandHandler)
    cmdThread = threading.Thread(target=cmdServer.serve_forever)
    cmdThread.start()
    logging.info('Command listener is running now ...')

    '''Start the web server. This can be done seperately, but it was kept here
    for testing purposes'''
    logging.info('Web server is starting ...')
    webserver.startWebServerLog()
    httpd = webserver.ThreadedHTTPServer(("", serverPort), webserver.WebServerHandler) 
    
    '''create a new thread to run the web server'''
    httpdThread = threading.Thread(target=httpd.serve_forever)
    httpdThread.start()
    logging.info('Web server is running at port {}'.format(serverPort))
     
    healthMsgSender.join()
    cmdThread.join()
    httpdThread.join()
    logging.info("Main thread in cloud agent DONE.")
