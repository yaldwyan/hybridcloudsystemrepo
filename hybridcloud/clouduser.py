'''
This module consists of a user request class.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''


class UserRequest():
    '''
    This class consists of all information needed to run a cloud application
    optimally and meet a required QoS. 
    '''
    def __init__(self, reqName, numVms, vmSize, 
                 minVms, maxVms, highLoadThr, lowLoadThr,volumeThr, 
                 responseTimeThr=None, userData=None, 
                 hasVolume=False, volumeSize=0, 
                 hasLb=False, lbPort=None, vmPort=None):

        self.requestName = reqName
        self.numVms = numVms
        self.vmSize = vmSize
        self.userData = userData
        self.volumeSize = volumeSize
        self.hasLb= hasLb
        
        if hasLb:
            self.hasLb = hasLb
            self.lbPort = lbPort
            self.vmPort = vmPort
            
        
        self.volumeSize = volumeSize
        self.hasVolume = hasVolume
        
        self.minVms = minVms
        self.maxVms = maxVms
        self.highLoadThr = highLoadThr
        self.lowLoadThr = lowLoadThr
        self.volumeThr = volumeThr
        self.responseTimeThr = responseTimeThr
              
if __name__ == '__main__':
    pass