'''
This module contains all configurations needed to run the cloud manager.


@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

import os.path, threading

'''This flag is needed to shut down all running services (threads) in the cloud
manager.'''
exitFlag = False
'''IP address of the cloud manager'''
cloudManagerIp = 'YOUR_CLOUD_MANAGER_IP'
'''Cloud manager directory'''
HYBRID_CLOUD_DIR = 'YOUR_HYBRID_CLOUD_DIRECTORY'
'''You can use, os.path.expanduser('~/hybridcloud')'''
''' A port on which the cloud manager listens to health messages from the cloud
 agent'''
MONITORING_PORT = 9034
''' A port on which a load balancer listens to commands from the cloud
manager''' 
LB_COMMAND_PORT = 8943
''' A port on which the cloud agent listens to commands from the cloud
manager''' 
VM_COMMAND_PORT = 8945

#APP_COMMAND_PORT = 8955

'''VM status'''
RUNNING = 'running'
CRASHED = 'crashed'
FAILED  = 'failed'
IDLE = 'idle'

'''Health status'''
HEALTHY = 'healthy'
UNHEALTHY = 'unhealthy'

'''Virtual Machines' sizes'''
VM_SMALL = 'small'
VM_MEDUIM = 'medium'

'''Cloud vendors'''
NECTAR_CLOUD='nectar'
AMAZON_CLOUD='amazon'
GOOGLE_CLOUD='google'
''' A new vendor name should be added here, if you want to implement a new
cloud platform '''

'''Cloud types'''
PRIVATE = 'private'
PUBLIC = 'Public'

'''Cloud manager here create unique IDs for any service in its system'''
class UniqeID():
    _UID = 0

serviceLock = threading.Lock()
def getUId():
    with serviceLock:
        UniqeID._UID += 1
    return "ID_" + str(UniqeID._UID)

def exitCloudManager():
    global exitFlag
    exitFlag = True
    
if __name__ == '__main__':
    pass
