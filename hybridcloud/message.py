'''
This file contains message types and keys.

@author: Yasser Aldwyan
@contact: yasser.dwyan@gmail.com
'''

MESSAGE_TYPE = 'messagetype'
IP = 'ip'
ID = 'id'
'''Message types'''
REGISTER_VM_MSG = 'registervmmsg'
UNREGISTER_VM_MSG = 'unregistervmmsg'
VM_HEALTH_MSG = 'vmhealthmsg'
TEST_MSG = 'testmsg'
SERVER_SLEEP_TIME = 'serversleeptime'

BACKEND_VMS = 'backendvms'

'''Monitoring keys'''
CPU_UTILIZATION = "cpuutilization"
MEMORY_UTILIZATION = "memoryutilization"
USED_VOLUME_SPACE = "usedvolumespace"
CPU = 'cpu'
MEMORY = 'memory'
VOLUME = 'volume'
SERVER_SLEEP_VALUE = 'serversleepvalue' 


